---
title: Memoria de Coordinación TIC - 2022-2023
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [TIC,IES,LaSenia]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\tableofcontents

\newpage
# Introducción

En esta memoria de Coordinación TIC se presentan algunas de las actuaciones que se han llevado a cabo a lo largo del Curso 2022-2023, así como las motivaciones y propósitos de las mismas.

Esta pequeña memoria no pretende ser un diario exhaustivo de todas las actuaciones, pero sí que puede servir para dar una idea del conjunto global, así como presentar una serie de Valores que pueden ayudarnos a comprender porqué se han tomado unas determinadas decisiones, que *a priori* parecen ir a contracorriente (y en muchos casos, lo hacen).

# El Software Libre y sus valores como premisa

En una sociedad cada vez más digitalizada, pero a la vez cada vez más ajena a cómo funciona en realidad el mundo digital, resulta de especial valor pedagógico dar ejemplos y valores que tal vez no estén siendo respaldados por el conjunto de la sociedad.

Los valores morales del software libre se basan en una filosofía ética y social que promueve ciertos principios y libertades fundamentales en el ámbito del software. Estos valores son defendidos por la comunidad del software libre y han sido promovidos por la **Free Software Foundation (FSF)** y otras organizaciones. 

Los principales valores morales del software libre son los siguientes:

 * Libertad: El software libre promueve la libertad de los usuarios para utilizar, estudiar, modificar y distribuir el software según sus necesidades. Los usuarios tienen la libertad de ejecutar el programa para cualquier propósito.
 * Acceso al código fuente: El software libre proporciona el acceso al código fuente del programa, lo que permite a los usuarios estudiar cómo funciona el software y realizar modificaciones o mejoras.
 * Distribución: Los usuarios tienen la libertad de distribuir copias del software a otras personas, lo que fomenta la colaboración y la comunidad.
 * Cooperación: La filosofía del software libre alienta la colaboración entre los desarrolladores, permitiendo que el conocimiento y las mejoras sean compartidos libremente para el beneficio de todos.
 * Transparencia: El acceso al código fuente y la posibilidad de modificarlo permite a los usuarios tener una mayor transparencia en el software que utilizan, lo que aumenta la confianza en su funcionamiento.
 * Respeto a la privacidad: El software libre no suele incluir características que recopilen datos privados de los usuarios sin su conocimiento o consentimiento.
 * Igualdad: El software libre brinda a todas las personas la misma libertad y acceso, sin discriminación por razones económicas, sociales o culturales.
 * Sostenibilidad: El software libre puede ser mantenido y mejorado por una comunidad de desarrolladores, lo que ayuda a evitar la obsolescencia y prolonga la vida útil del software.
 * Educación: El software libre fomenta la educación y el aprendizaje, ya que permite a los usuarios estudiar y comprender cómo funcionan los programas.
 * Independencia tecnológica: Al utilizar software libre, los usuarios no quedan atados a un proveedor específico y tienen la capacidad de elegir y adaptar las herramientas tecnológicas según sus necesidades.

Estos valores morales del software libre buscan promover una sociedad más justa, colaborativa y con una mayor democratización del conocimiento tecnológico.

Al igual que se potencian el respeto y la igualdad en las aulas por parte de la administración y la comunidad educativa (como no se podría entender de otras manera), mientras que la sociedad muestra una feroz competitividad. Creemos que desde la Coordinación TIC se deben fomentar los valores y el uso del Software Libre para demostrar, que es posible no depender de empresas externas que no fomentan estos valores y disfrazan sus verdaderos propósitos bajo una idea de "efectividad" e "inmediatez".

Al enseñar en el IES las diferentes filosofías del software libre y cómo estas se aplican en la educación y la tecnología, se pueden promover diversos valores pedagógicos que enriquecen el proceso educativo. Algunos de estos valores son:

 * **Participación y colaboración**: Al introducir conceptos sobre el software libre, se alienta a los estudiantes a trabajar en proyectos colaborativos, compartiendo conocimientos y contribuyendo activamente en la creación y mejora de recursos educativos y tecnológicos.
 * **Creatividad y pensamiento crítico**: Fomentar el uso de software libre puede inspirar a los estudiantes a ser más creativos, al permitirles acceder al código fuente y personalizar las herramientas tecnológicas según sus necesidades y objetivos educativos. Además, esto estimula el pensamiento crítico al analizar cómo funcionan los programas y cómo pueden mejorarse.
 * **Autonomía y empoderamiento**: Al utilizar software libre, los estudiantes pueden ejercer su autonomía al elegir y utilizar herramientas tecnológicas que consideren más adecuadas para sus proyectos y aprendizaje. Esto los empodera para tomar decisiones informadas y responsables en el ámbito digital.
 * **Conciencia ética y social**: Al aprender sobre la filosofía del software libre, los estudiantes pueden desarrollar una mayor conciencia de la importancia de la libertad, la privacidad, la igualdad y el acceso a la educación y la tecnología para todos. Esto puede motivarlos a considerar las implicaciones éticas y sociales de sus elecciones tecnológicas y de cómo estas afectan a la comunidad en general.
* **Aprendizaje basado en proyectos y problemas**: Introducir el software libre en proyectos educativos puede ser una forma efectiva de aplicar los conocimientos teóricos en situaciones prácticas y reales. Los estudiantes pueden trabajar en proyectos significativos, desde el desarrollo de software hasta la creación de recursos educativos, aplicando los valores del software libre en la práctica.
* **Promoción de la cultura de compartir**: El software libre fomenta el compartir conocimientos y recursos, lo cual puede llevar a crear una cultura de colaboración en el aula y más allá. Los estudiantes pueden aprender a valorar y respetar el trabajo de los demás, así como a contribuir generosamente con sus propios conocimientos.
* **Desarrollo de habilidades técnicas**: Al tener acceso al código fuente y la posibilidad de modificar el software, los estudiantes pueden desarrollar habilidades técnicas en programación y desarrollo de software, lo que les brinda ventajas en el mundo laboral y en el uso responsable de la tecnología.
* **Fomento de la educación abierta**: Los valores del software libre se alinean con la educación abierta, que busca el acceso libre y gratuito a los recursos educativos. Esto puede inspirar a los estudiantes a participar en movimientos de educación abierta y compartir sus conocimientos con una audiencia más amplia.

Enseñar a los alumnos sobre la filosofía del software libre no solo promueve una mentalidad más abierta y colaborativa en el ámbito tecnológico, sino que también puede inculcar valores fundamentales que los preparan para ser ciudadanos informados y éticos en una sociedad digital en constante evolución.

Estos valores se han tenido en cuenta y se han implementado en toda la Infraestructura Software y Hardware del Instituto. Poniendo al alcance de todos el código fuente de los diferentes paquetes que configuran las máquinas del centro y la organización lógica del mismo usando un repositorio de Software público y protegido mediante una licencia GPL.

\newpage
# Instalaciónes del centro

## Configuración de la Red del Centro

### Red de Secretaria

Hay una serie de bocas de todos los switches Huawei del centro que se encuentran en la red de secretaria.
En esta red funciona un DHCP no gestionable y que tiene un filtrado diferente del de las aulas. 

No podemos conectar equipos a esta red sin abrir tickets.

### Red del Centro (Aulas)

Esta Red es la definida por la Dirección IP:

*172.29.0.0/23*

Nótese que nos hallamos en una *Doble C* para l@s fans de las categorizaciones
de las Redes en clases, pero que además no importa los dos primeros bits del primer octeto, así que de nuevo,
lo que importa no es cómo las clasificamos sino cómo funcionan ;-) .

Disponemos de 510 direcciones IP válidas en esa red:

172.29.0.1 $\rightarrow$ 172.29.1.254

Sin embargo, para facilitar la administración del centro y ya que no contamos con un DHCP propio ( y ser de esta 
manera más *Consellería-compliant*) lo que está configurado en el Switch del centro (Gestionado por el SAI), es que esta red de 510 direcciones 
está separada en dos mitades, la primera :

- `172.29.0.0/24` $\rightarrow$ NO TIENE DHCP, de esta manera nosotros ponemos IPs fijas en esta red para los equipos de las Aulas del centro.
- `172.29.1.0/24` $\rightarrow$ TIENE DHCP, donde los portátiles, móviles, y dispositivos no configurados en la red del centro se les asigna una dirección
   y pueden trabajar.

Esto será cambiado a lo largo del curso 23-24 debido a la normativa de espacios públicos de la Consellería. 

## Aulas de Informática

En el centro contamos con una serie de Aulas de Informática y Aulas de docencia que se describen a continuación:

## Aulas de Informática I y II

Estas aulas cuentan con Switch plano, un ordenador del profesor que hace de servidor de aula. Todos los equipos cuentan con Xubuntu 22.04 instalado y actualizado con el metapaquete de la Senia.

Redes:

* Aula 1: 192.168.1.*/24
* Aula 2: 192.168.2.*/24

## Aulas de Informática de Ciclos Formativos de Informática

Contamos con 3 aulas de Informática, donde el ordenador del profesor es uno más de la red, sin hacer de enlace sobre la red de Aulas.

Además contamos con un servidor que da servicio al centro (*a.k.a Servidor del Aula 4), que hace de enrutador de las tres aulas hacía la red del centro.

Estas tres aulas tienen la red:

* Aulas: 192.168.4.*/23

Y los equipos tienen una reserva de MAC en este servidor del Aula 4, haciendo **supernetting**, si se desean comprobar más detalles, se puede encontrar información relativa a todos los equipos desde aquí:

* [ Infraestructura ](https://gitlab.com/aberlanas/senia-cdd/-/blob/master/docs/Infraestructura.md)

\newpage
## Aulas de docencia del Centro

Todas instaladas con la distribución Xubuntu + `senia-cdd-xfce`. 

Estas máquinas cuentan con una dirección IP fija dentro del rango estático del Router Teldat, con el propósito de facilitar el mantenimiento usando `ssh`. 

Ya que cuando el profesorado reporta una incidencia de un aula, sabiendo su número podemos establecer su IP y conectarnos para arreglarlo con la mayor brevedad posible.

\newpage
# CDC y Competencia Digital Docente

Nuestro centro forma parte de la red de Centros Digitales Colaborativos, cuya formación se impartió a lo largo del curso 2021-2022 y que durante este año y dentro del PAF se continuó con el propósito de profundizar más en herramientas que facilitaran y empoderaran al claustro para el uso de las herramientas digitales disponibles. 

## PAF

Fué impartido por miembros del Departamento de Informática, se creó un curso de Aules en la categoría correspondiente, se plantearon una serie de actividades prácticas y se promovió su utilización desde el primer momento, ya que este tipo de formación es mucho más útil cuando lo aprendido se pone en marcha lo antes posible.

Se hizo hincapié en utilidades no sólo de Microsoft, ya que aunque la Consellería ofrece una suite de herramientas, no están adaptadas a los docentes y además dentro de los valore expuestos anteriormente, creemos que vale la pena mostrar y enseñar que existen un montón de herramientas que en entornos mixtos, privativos o enteramente libres, pueden facilitar la labor docente y permitir un mejor uso de la tecnología.

Algunas de las que tuvieron más éxito:

- Lumi (H5P)
- Instalación de Tipografías (OpenDyslexic)

## Competencia Digital Docente

Se realizaron todas las reuniones propuestas por el asesor y se incorporaron las estrategias y los objetivos dentro del Plan Digital del Centro que será añadido a la Programación General Anual para el curso 2023-2024.

Esperamos que para el comienzo del curso se cuente con la normativa ya aprobada y las actuaciones que se realicen tengan un marco normativo regulador que impulse la Competencia Digital Docente para que la Comunidad Educativa vaya avanzando en las diferentes destrezas digitales previstas dentro del **Marco Común Europeo de Referencia**.

\newpage
# Aules y Formación

Desde antes de la Covid-19, el centro viene usando Aules como plataforma de apoyo a la docencia, al tratarse de un Moodle (uno de los proyectos de Software Libre más famosos), cuenta con todo el respaldo de la comunidad y la simpatía de la Coordinación TIC del centro.

 * [ Aules ](https://portal.edu.gva.es/aules/)

Sin embargo, cabe destacar que durante este curso, las funcionalidades disponibles para la resolución de incidencias dentro de Aules para el Coordinador TIC del centro (**Administraciones Delegadas de Aules**), han sido mermadas.

A lo largo del curso, fueron añadiendo funcionalidades que esperamos que se mantengan cara al curso que viene para facilitar la labor del Coordinador TIC y poner en valor que estas herramientas funcionan perfectamente como apoyo a la docencia en cualquier nivel de la educación secundaria y en la formación profesional.

\newpage
# Líneas de actuación

A continuación se detallan algunas de las actuaciones que han tenido lugar a lo largo del curso.

## Actuaciones regladas por parte de Conselleria

Respecto a las funciones de Coordinación TIC regladas por parte de Consellería:

- Se ha mantenido actualizado el Inventario de SAI.
- Se han puesto el menor número de incidencias posibles, intentando siempre que fuera posible resolver los problemas por nuestra cuenta con dos principales propósitos:
	- Mejor uso de los recursos de GVA (contamos con Ciclos de Informática y con un profesorado dispuesto a ayudar siempre).
	- Cuando el alumnado es el que resuelve (o ayuda a resolver) las incidencias del centro, se le hace partícipe de la comunidad 
	  educativa, dándole valor y autonomía, y mediante todos los valores que se han descrito anteriormente y gracias a que todo 
	  es Software Libre, el alumnado aprende a resolver los problemas por si mismo, leyendo y comprendiendo *cómo funcionan* las cosas.
- Se le ha dado soporte técnico y pedagógico al claustro.
- Se han organizado los portátiles en los diferentes carritos que llegaron, dotando a cada piso del IES de un aula móvil.
- Se ha realizado un mantenimiento de los portátiles.
- Se han prestado equipos y tabletas usando los mecanismos adecuados.
- Se han puesto incidencias cuando la situación o el problema no se podía resolver por nosotros mismos.
- Y muchas más acciones dentro del contexto del centro, siempre con el objetivo de ayudar a la comunidad educativa (alumnado y claustro).

## Actuaciones no regladas

- Instalación y configuración de la Red Wifi del centro para dar soporte a las Aulas Móviles que fueron puestas en marcha por parte de Consellería.
- Mejora del Hardware del Aula 3 de Informática para impartir el Ciclo de ASIR, del que no ha llegado la dotación.
- Formación al claustro dentro del PAF para un mejor aprovechamiento de los recursos TIC del centro. 
- Soporte a los diferentes departamentos para la instalación y uso de los Libros Digitales de las Editoriales.
- Apoyo a la *XarxaLlibres*, actualizando el Software, realizando mantenimiento de la BBDD, etc.
- Apoyo al desarrollador (Antonio Calabuig) de la plataforma de realización de Informes personalizados del IES la Senia (**docusenia**).

\newpage
# Final de curso y nuevos retos

En este final del curso se han realizado dos cambios muy importantes que tendrán consecuencias a lo largo del curso 2023-2024:

- Instalación de las Pantallas Digitales Interactivas en la mayoría de las Aulas del Centro.
- Cambio de la Infraestructura de Red por parte de Consellería, con dos proyectos en marcha:
	- Escuelas Conectadas (WiFi).
	- Proyectos Fenix (Cableado).

A lo largo del verano esperamos elaborar algunas herramientas y protocolos que nos sirvan para facilitar el uso de estas herramientas, y así como un correcto uso de las mismas.


# Bibliografía, Recursos y Webgrafía

 * [ Código fuente de la Senia ](https://gitlab.com/aberlanas/senia-cdd/)
 * [ Repositorio de la Senia (PPA)](https://launchpad.net/~ticsenia/+archive/ubuntu/ppa/+packages)
 * [ Xubuntu ](https://xubuntu.org/)
 * [ Aules ](https://portal.edu.gva.es/aules/)
 * [ OpenDyslexic](https://opendyslexic.org/)
 * [ Lumi ](https://app.lumi.education/)
