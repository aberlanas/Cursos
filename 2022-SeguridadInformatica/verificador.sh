#!/bin/bash

# Simple verificador

SHAISO=$(sha256sum ubuntu-21.04-live-server-amd64.iso| cut -d " " -f1)
SHAURL=$(wget https://old-releases.ubuntu.com/releases/21.04/SHA256SUMS -O /tmp/sha && cat /tmp/sha  | tail -n 1| cut -d " " -f1)

echo "ISO: $SHAISO"
echo "URL: $SHAURL"

if [ "$SHAISO" = "$SHAURL" ]; then
	echo " Las Sumas Coinciden "
fi


exit 0
