---
title: Práctica 03 Verificación de Integridad
subtitle: UD1. Fundamentos de Seguridad Informática
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [Seguridad Informatica]
titlepage: false,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Practica 03 :Verificacion de Integridad

## Paso 1: Descarga de la Imagen de Ubuntu 

Nos descargamos de la página oficial del proyecto Ubuntu, la imagen de instalación de Ubuntu Server

- `ubuntu-21.04-live-server-amd64.iso`

![Lista de Ficheros](imgs/practica03-01-descargas.png)\

![Descarga de la ISO](imgs/practica03-02-descargas.png)\


## Paso 2 : Obtención de la suma de verificación

Usando `curl` obtenemos el contenido del fichero de la página que contiene las Sumas de verificación del 
desktop y del servidor



![SHA256Sum](imgs/practica03-03-curl.png)\

## Paso 3 : MiniScript de verificación

Hemos creado un pequeño script en Shell que comprueba que la suma de verificación del fichero y 
la que se nos indica en el fichero `SHA256SUMS` son la misma.


![Ejecución del Script](imgs/practica03-05-script.png)\

Adjunto el Script en formato texto:

```shell

#!/bin/bash

URLAUX="https://old-releases.ubuntu.com/releases/21.04/SHA256SUMS"

# Simple verificador

SHAISO=$(sha256sum ubuntu-21.04-live-server-amd64.iso| cut -d " " -f1)
SHAURL=$(wget "$URLAUX" -O /tmp/sha && cat /tmp/sha  | tail -n 1| cut -d " " -f1)

echo "ISO: $SHAISO"
echo "URL: $SHAURL"

if [ "$SHAISO" = "$SHAURL" ]; then
	echo " Las Sumas Coinciden "
fi

exit 0

```




