---
title: Práctica 04 Auditoría de Contraseñas
subtitle: UD1. Fundamentos de Seguridad Informática
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [Seguridad Informatica]
titlepage: false,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Práctica 04 Auditoría de Contraseñas

## Practica 1: Ataque por diccionario


Yo utilizo Ubuntu como sistema base, así que después de haber instalado `hashcat` utilizando `apt` y habiendo separado 
el fichero con las contraseñas en 3 ficheros diferentes, he realizado los siguientes comandos para obtener los passwords
utilizando un diccionario.

![Instalación de hashcat](imgs/practica04-hashcat-01.png)\

Ejecución del comando:

```shell

hashcat --show -m 1800 -a 0 hashes_1.hp 500_passwords.txt

```

![Ejecución de hashcat](imgs/practica04-hashcat-02.png)\

En la imágen se ve el parámetro `--show` porque ejecuté el comando la primera vez sin el --ignore-potfile... y almacenó los passwords.

\newpage
## Practica 2: Ataque por fuerza bruta

Para realizar un ataque por fuerza he ejecutado el siguiente comando:

OPCION="?1?1?1?d?d?d"

hashcat --potfile-disable -m 1800 -a 3 hashes2.hp $OPCION –i –increment—min 6 increment—max 6 -o practica4.2-passRecover.txt 


Este comando que utiliza sólo la fuerza bruta para la obtención de los passwords, así que aunque se le ha dicho que utilice 
directamente palabras compuestas por 3 carácteres y 3 dígitos, le puede costar mucho.

Si se muestra el status de la ejecución:

![Estado de la ejecución](imgs/practica04-hashcat-05.png)\

Se puede comprobar que el progreso recorrido es muy pequeño y que se están probando todas las palabras de manera correcta.


\newpage
## Practica 3: Ataque híbrido

Para realizar un ataque híbrido he ejecutado el siguiente comando:

```shell
hashcat --potfile-disable -o practica4.3-passRecover.txt -m 1800 -a 6 hashes_3.hp 500_passwords.txt 20?d?d 
```

A continuación se muestran las capturas de la ejecución y  del resultado.


![Ejecución de hashcat](imgs/practica04-hashcat-03.png)\


![Ejecución de hashcat](imgs/practica04-hashcat-04.png)\


