---
title: Propuesta de Curso para el PAF - 2022-2023
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: false,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Propuesta de Curso para el PAF 2022-2023

## Introducción

Durante el curso 2021-2022 en el IES La Senia se realizó un estudio **SELFIE**, para conocer el grado 
de Competencia Digital docente tanto del profesorado como del alumnado y el equipo directivo. 

Al analizar los resultados de dicho informe se encontraron una serie de fortalezas y de debilidades por
parte del profesorado a la hora de utilizar los *recursos TIC* dentro de su labor docente. 	

Con el objetivo de facilitar esta labor docente y fomentando dentro del profesorado la adquisición
de unas competencias digitales **reales** y adaptadas a sus necesidades, se propone un curso de formación
dentro del **PAF** del Centro con los contenidos que se describen a continuación.

## Contenidos

* Introducción a la formación.
* Uso de las plataformas y portales de la GVA para la docencia:
	- Itaca3
	- Informes
	- Itaca - Docent
* Uso de las Utilidades TIC en el Centro:
	- Owncloud.
	- Redes Wifi.
	- Incidencias.
* Docencia Online
	- Aules
* Correo Electrónico.
* Identidades digitales.
* Documentos, formatos y utilidades.
* Firma Digital y Certificados. 
* Creación de Recursos Educativos.

\newpage

## Metodología

* Curso Online con tareas.
* Clases presenciales/online.
* Video-tutoriales.

## Ponentes

- Patxi Sanchis Luis \<fj.sanchisluis@edu.gva.es\>
- Angel Berlanas Vicente \<a.berlanasvicente@edu.gva.es\>
	





