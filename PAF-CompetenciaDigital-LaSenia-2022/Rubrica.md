---
title: Rúbrica Curso para el PAF - 2022-2023
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\tableofcontents

# Rúbrica y Seguimiento de las sesiones

Dentro del plan de Formación del Centro, se debe realizar un seguimiento de los contenidos del curso, así como de las tareas que se irán presentando a lo largo de las sesiones.

Para poder realizar este seguimiento se han diseñado una serie de instrumentos que nos permitirán tomar consciencia del grado de éxito del curso, así 
como de las diferentes destrezas y conocimientos que se han presentado en el curso.

## Aules y Formación

El curso contará con un curso preparado en Aules Docent, donde los ponentes del curso colgarán diferentes recursos, actividades, refuerzos, vídeos, ...

![Aules Docent](./imgs/aules-docent.png)\

---

### Seguimiento

Al utilizar Aules un Moodle (el entorno de *e-Learning* más potente y **libre**), contamos de base con un montón de tipos de herramientas, propuestas didácticas, categorías, 
tareas, etc...que permitirán al profesorado descubrir las posibilidades que tiene y brindarles la posibilidad de que lo incorporen a la práctica docente. 

Con esta propuesta, esperamos mejorar uno de los puntos descubiertos en el análisis DAFO: la falta de incorporación de las nuevas herramientas a la práctica
docente, ya que al ser el alumnado de este curso los miembros del claustro y situarlos en el papel de alumnos, podrán observar e interactuar con las diferentes 
propuestas didácticas que se les irán planteando.

A continuación se muestran algunas de las herramientas que se utilizarán en el curso de Aules.

### Competencias 

Aules cuenta con seguimiento de Competencias que se utilizará para comprobar el grado de adquisición de las mismas a lo largo del curso.

----

\newpage
### Rúbricas

Las actividades se corregirán mediante rúbricas (e incluso, se plantea a lo largo del curso como actividad que los docentes elaboren una para uso en sus cursos).
Estas rúbricas se aplicarán (con pequeñas modificaciones si la práctica lo requiere) a todas las tareas que se plantearán en el curso. 

A continuación se presenta el *esqueleto* de las rúbricas que se utilizarán a lo largo del curso.

| Criterios o Indicadores | Nivel de Logro: Excelente | Nivel de Logro: Adecuado | Nivel de Logro: Insuficiente |
|-------------------------|---------------------------|--------------------------|------------------------------|
| Comprensión de la tarea | 						  |                          |                              |
| Ejecución de la tarea   |                           |                          |                              |
| Incorporación de nuevas destrezas |                 |                          |                              |
| Creatividad             |                           |                          |                              |
| Presentación            |                           |                          |                              |

Esta rúbrica se definirá en Aules y se podrá utilizar a lo largo de curso, además se mostrará al profesorado, de tal manera que podrán saber **qué es** lo que se 
pretende con cada una de las diferentes prácticas y cómo se evaluará. Dando valor en cada una a los puntos más importantes.

Por supuesto que se trata de un modelo flexible y se adaptará a cada una de las actividades que se irán planteando. Al igual que en las programaciones 
didácticas, debemos ser capaces de medir diferentes aspectos dependiendo del momento y la situación. 

----

### Retroalimentación

Aules cuenta con un estupendo sistema de retroalimentación que nos servirá para enviarles información al los docentes a medida que se vayan 
revisando las diferentes actividades.

----


### Cuestionarios de Auto-Evaluación

Tras cada bloque de contenidos se pondrá a disposición de los docentes un pequeño cuestionario a partir de una batería de preguntas que 
le servirán para comprobar que los contenidos que se explican en las diferentes tareas y recursos están siendo adquiridos.

----

## Sesiones Presenciales

Antes de cada sesión se avisará mediante el **Tablón de Anuncios** del Curso de Aules, cuales serán los temas que se tratarán en la sesión presencial, con el objetivo 
de dar *flexibilidad* a los docentes para que puedan seguir las sesiones desde su casa o si necesitan más apoyo acercarse al IES, donde se estará realizando la sesión 
para contar con ayuda y asistencia de los ponentes a la hora de plantear las actividades.

Al tratarse de sesiones presenciales mixtas, se realizará una hoja de firmas que se pasará a los miembros presencialmente y que será completada
con los asistentes a la sesión mediante videoconferencia utilizando *MS Teams*.



