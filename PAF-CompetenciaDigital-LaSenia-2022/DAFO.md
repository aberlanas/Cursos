---
title: DAFO Curso para el PAF - 2022-2023
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: false,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# DAFO del Curso para el PAF 2022-2023

La realización de un análisis DAFO de la situación del IES La Sénia nos permitirá aportar puntos de vista diferentes y ser más conscientes de lo que podemos, o debemos trabajar y mejorar a lo largo de esta formación.

## Debilidades (D)

A continuación se exponen una serie de debilidades detectadas, ya sea mediante la observación directa, así como a través del uso de la herramienta *SELFIE* que se utilizó el año pasado.

Dentro del contexto del IES una de las Debilidades (que esperamos sea resuelta en breve), es la **antigüedad de las instalaciones del centro**.  Esto conlleva una serie de problemas de infraestructura en las aulas a la hora de la conectividad, tanto cableada (lo ideal) como mediante redes inalámbricas (*WiFi*). La disposición de los diferentes barracones, así como el gimnasio y el taller de Tecnología, obligan a una instalación de cableado estructurado que sin duda presenta posibilidades de mejora.

Otra de las debilidades del centro es la ausencia de dotación de ordenadores fijos en las aulas en condiciones de seguridad y adecuación, que permitan al profesorado su uso y mantenimiento de manera adecuada, contando tan sólo con algunos estantes pegados a la pared al lado de la pizarra que tienen multitud de cables y conexiones.

## Amenazas (A)

La **falta de formación y de interés por parte del profesorado** a la hora de utilizar las herramientas TIC de manera adecuada da lugar a errores y al desánimo cuando algo no funciona exactamente como se desea o se producen errores informáticos, ya sea por causas del usuario o de las diferentes herramientas proporcionadas por la GVA.

Se ha observado una carencia de gestión autónoma al **tratar los problemas que aparecen en el uso de la tecnología**.

El uso de las tecnologías como herramientas lúdicas o de gamificación sin un verdadero propósito educativo más allá de que 
el alumnado compita en *Kahoots*, da lugar a una visión del mundo de las TIC que está muy alejada de la realidad.


## Fortalezas (F)

Existe cada vez más la consciencia entre los miembros de la comunidad educativa de que **un buen uso de las TIC, no sólo es necesario sino que además facilita nuestra labor como docentes y mejora la comunicación con el alumnado y las familias**, permitiendo así una mayor integración del centro en la comunidad.

Se trata de un claustro que trabaja muy bien en equipo, donde reina el buen ambiente y las propuestas son escuchadas con atención y respeto.

## Oportunidades (O)

Teniendo en cuenta los puntos descritos anteriormente, un buen enfoque y punto de partida podría ser el uso de estas herramientas de las que disponemos para realizar la labor docente de una manera más adecuada, amable y sencilla.

Transmitir confianza al profesorado a la hora de  resolver los problemas que seguro le aparecerán a lo largo del uso de las tecnologías, le ayudarán no sólo en la labor en el centro sino dentro de un marco de crecimiento personal.  

	





