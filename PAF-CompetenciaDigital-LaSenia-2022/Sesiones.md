---
title: Propuesta de Curso para el PAF - 2022-2023
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: false,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Propuesta de Sesiones para el Curso PAF 2022-2023

Se plantea una formación de *30 horas*, de las cuales el 30% serán con ponentes: 10 sesiones.

## Temporalización y contenidos

| Fecha | Contenido | Ponente | Horas | Horas Acumuladas |
|-------|-----------|---------|-------|------------------|
| 15-11-2022| Introducción y presentación de los contenidos | Sí | 2 | 2|
| 22-11-2022| Plataformas y portales | Sí| 2 | 4|
| 23-11-2022| Actividades sobre Itaca3 | No| 2 | 6| 
| 29-11-2022| Docent e Informes | No | 2 | 8 |
| 30-11-2022| Itaca Docente 	| No | 2 | 10 |
| 14-12-2022| Utilidades TIC del Centro | Sí | 2 | 12 |
| 21-12-2022| Owncloud e Incidencias | No | 2| 14|
| 08-01-2023| Redes Wifi y dispositivos | No | 2 | 16| 
| 10-01-2023| Docencia Online | Sí | 2 | 18|
| 12-01-2023| Aules I 	      | No | 2 | 20|
| 15-01-2023| Aules II        | No | 2 | 22|
| 17-01-2023| Correo Electronico y iD| Sí | 2 | 24|
| 22-01-2023| Documentos, Formatos y utilidades | No | 2 |26|
| 25-01-2023| Firma digital y Certificados | No | 2 | 28|
| 31-01-2023| Creación de Recursos Educativos | Sí| 2 | 30 |


## Ponentes

- Patxi Sanchis Luis \<fj.sanchisluis@edu.gva.es\>
- Angel Berlanas Vicente \<a.berlanasvicente@edu.gva.es\>
- Diego García Tarín \<d.garciatarin@edu.gva.es\>	





