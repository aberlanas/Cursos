---
title: Apuntes de POO
subtitle: IES La Senia
author: Angel Berlanas Vicente, Patxi Sanchis Luis
header-includes: |
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: false,
page-background: "../../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents

\newpage

# Introducción

Vamos a generar una pequeña clase `Juego.java` que va a ir cargando los diferentes
personajes que necesitaremos para ir incrementando poco a poco el nivel de complejidad
y además ir presentando los diferentes conceptos de la Programación Orientada a Objetos.

Para ello utilizaremos una clase `Personaje.java` que iremos desarrollando en las sesiones
y que iremos expandiendo, haciendo más compleja, etc. (como en el mundo real).


\newpage
```Java
import java.util.*;

public class Juego{

	public static void main (String args[]){
		// Creamos un personaje mago
		Personaje mago = new Personaje();
		Personaje barbaro = new Personaje();
	
		// Iremos ampliando esta clase con todo 
		// lo necesario a lo largo de las sesiones
		// pero por ahora tenemos algo.
	}
}

```


\newpage

# Abstracción


```Java

public class Personaje{

	int puntosDeVida;
	int fuerza;
	int constitucion;
	int agilidad;
	int sabiduria;
	int inteligencia;
	int carisma;
	String nombre;
	String raza;
	String profesion;
	String armaEquipada;

	public int tiraDados(int nDados){
		int result=0;
		for (int i=0;i<nDados;i++){
			result+=(int)Math.random()*6+1;
		}
		return result;
	}
	
	public Personaje(){
		System.out.println(" * Creando un personaje ");
		// Aleatoriamente generar 6 tiradas de 3 dados y asignarlos a los atributos.
		
		this.fuerza=tiraDados(3);
		//....
		
		
	}

	public void setProfesion(String profesion){
		this.profesion=profesion
		
		// Seleccionar los dos resultados mas altos 
		// en funcion de la profesion
		
		// mago = INT + SAB
		// guerrero = FUE + AGI
		// tanque = FUE + CON
		// ladron = AGI + SAB
		// bardo = CAR + AGI
		// profeta = CAR + SAB
		
		// el resto ponerlos de manera aleatoria
	
	}
	// Iremos ampliando esto pero por ahora tenemos algo con 	
	// lo que trabajar
}

```

\newpage

# Getters y Setters

```Java

public class Personaje{

	int puntosDeVida;
	int fuerza;
	int constitucion;
	int agilidad;
	int sabiduria;
	int inteligencia;
	int carisma;
	String nombre;
	String raza;
	String profesion;
	String armaEquipada;

	public Personaje(){
		System.out.println(" * Creando un personaje ");
	}

	// Getters y Setters
	public void setPuntosDeVida(int puntosDeVida){
		System.out.println(" * Estableciendo nuevos puntos de vida "+puntosDeVida);
		this.puntosDeVida = puntosDeVida;
	}			
	
	public int getPuntosDeVida(){
		System.out.println(" * Obteniendo los puntos de vida "+this.puntosDeVida);
		return this.puntosDeVida;
	}
	
	// Resto de Getters y Setters
	// ....
}

```

\newpage
## Tarea - peleaCon

* Ampliad la clase propuesta con Getters y Setters para todos los atributos.
* Cread el método `peleaCon` en la clase `Personaje`, que acepte como parámetro otro personaje y  
  desarrollad un pequeño enfrentamiento como deseeis.
* Desde la Clase `Juego` cread los dos personajes y realizad esa pequeña pelea programada.

Ejemplo de declaración de `peleaCon`

```Java

public void peleaCon( Personaje enemigo){
	// Fill  the gaps
}

```


\newpage
# Visibilidad

\newpage
# Responsabilidad

 
