---
title: Examen de Programación 2022/11/10
subtitle: Programación - IES La Sénia
author: Patxi Sanchis Luis
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: True,
page-background: "../../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents

\newpage

# Una tarde en la Feria

Desde niño me han fascinado las ferias de atracciones. Durante todo el
año guardaba en mi hucha algo de dinero para cuando llegaba la Navidad y
se montaba la feria de mi pueblo. Los amigos cogíamos todo el dinero que
teníamos y nos pasábamos las tardes disfrutando de las atracciones y
comiendo algodón de azúcar, castañas calentitas o palomitas de maíz.

\begingroup
\centering
![](imgs/feria.png){width="10.229cm" height="6.516cm"}\
\endgroup

Los parques temáticos han desvirtuado un poco el poder cautivador que
tenían las ferias con sus luces, sus atracciones más impresionantes y
los gritos de la gente al ganar algún premio. Por eso las ferias siempre
han necesitado ayuda para gestionar su funcionamiento. ¿Que tal lo
podrías hacer tú?

Nuestra pequeña feria tiene 4 atracciones. ¿Serás un programador
suficientemente bueno para hacer rentable nuestra feria?

\newpage

# 1. La tómbola "La Pepa"

En la tómbola *la Pepa* ***siempre toca***.

Por sólo un euro obtienes un boleto y ese boleto siempre tiene premio.

\begingroup
\centering
![](imgs/tombola.png){width="4.651cm" height="3.096cm"}\
\endgroup

Cada boleto tiene dibujado un naipe de la baraja española (oros, copas,
espadas y bastos). Cada uno de los boletos tiene premio pero hay boletos
con un premio especial.

En los últimos años se han renovado los premios y ésta es la lista:

a)  El **As de Oros** es el premio gordo, un móvil
b)  El **resto de los ases** tienen de premio un juego de mesa.
c)  El **Rey de Oros** tiene como premio un dron.
d)  El **resto de los reyes** tienen de premio un comic.
e)  Para el resto de los boletos recibes como regalo un dulce típico de
    Murcia, tierra de la gran Pepa: paparajotes, sequillos, cordiales,
    monas de limón, tortas de recao, libricos de Yecla, picardías,...

\newpage

## Entrada

La entrada estará compuesta de numerosos casos de prueba. Cada uno en una línea con la información del boleto: un *número* (del 1 al 12); y una letra *mayúscula*: O (Oros), C (Copas), E (Espadas) y B (Bastos).

## Salida 

Para cada caso de prueba se debe escribir en una línea el premio que le corresponde.


## Entrada de ejemplo


>1 O\
>12 E\
>3 B\
>1 C\
>12 O\

## Salida de ejemplo

>MOVIL\
>COMIC\
>DULCE MURCIANICO\
>JUEGO DE MESA\
>DRON\

\newpage

# 2. Los dardos *Dalton*

Todo el mundo conoce a los hermanos *Dalton*, cuatro
hermanos forajidos que intentaban jugársela al bueno de *Lucky Luke*, el
vaquero más rápido del viejo
Oeste.

\begingroup
\centering
![](imgs/dalton.png)\
\endgroup

Lo más curioso de los hermanos Dalton es que siempre iban en orden del
más bajo al más alto. Era lo único que los diferenciaba, el resto de su
aspecto era idéntico.

En homenaje a los hermanos Dalton tenemos una atracción en la que los
participantes tienen que lanzar 4 dardos, con la particularidad que
deben hacerlo en el orden correcto. ¿Podrás saber quien se lleva premio?

\newpage
## Entrada

La primera línea nos dirá cuántos participantes han lanzado sus dardos. 
Para cada uno existirá una línea que representan sus tiradas, cuatro valores que indican la puntuación de cada dardo (del 1 al 20). 
Para obtener premio, los puntos tienen que estar ordenados estrictamente de menor a mayor, como los hermanos Dalton.

## Salida

Para cada participante se indicará si obtiene premio o debe volver a intentarlo.

## Entrada de ejemplo

>4\
>3 8 12 18\
>16 17 18 20\
>7 7 11 13\
>20 8 5 11\

## Salida de ejemplo


>PREMIO\
>PREMIO\
>VUELVE A INTENTARLO\
>VUELVE A INTENTARLO\

\newpage
# 3. El *Ángel* de la Muerte

En todas las ferias hay una atracción a la que todo el
mundo teme: la más impresionante y terrorífica de todas. Solo las
personas más valientes se enfrentan a
ella.

\begingroup
\centering
![](imgs/angel.png){width="4.651cm" height="3.096cm" }\
\endgroup

Todo el mundo quiere subir pero nadie se atreve. La gente dice cosas
como: "es que he comido mucho y ahora no voy a subir", "si me acompañas
subo, yo sola no voy", "no he traído ropa interior de recambio", "JAJAJA".
Pero una vez has dado el paso y estás en la cola para subir el estomago
te da vueltas, se te seca la garganta, los nervios te atenazan el
cuerpo. Pero no todas las personas podrán enfrentarse al **Ángel de la Muerte**.

Para evitar problemas y controlar el acceso hay una máquina que controla
el límite de *altura* y de *edad. *Esta máquina detecta una altura
mínima de 1,50 m y con la fecha de nacimiento calcula la edad que debe
ser de 16 años o más.

\newpage

## Entrada

La primera línea nos dirá cuántos valientes han decidido ponerse en la cola. 
Para cada uno habrá una línea con dos datos: **h**, que representa la altura en cm de cada persona y una fecha **dd/mm/aaaa**, que indica la fecha de nacimiento de cada una.

## Salida

El programa mostrará el número de personas que pueden acceder a la *superatracción*.

## Entrada de ejemplo

>6\
>175 15/04/2005\
>163 22/04/1991\
>150 01/11/2006\
>194 01/07/2007\
>188 31/12/1970\
>146 28/02/2001\

## Salida de ejemplo

>4\

\newpage
# 4. *Patxi*, el pistolas

Las atracciones de tiro son de las más antiguas y tradicionales en las
ferias. Sea con ballestas, rifles o pistolas todo el mundo quería probar
su puntería y también es una manera más pacífica de decidir quién
dispara mejor o quién es más rápido disparando.

\begingroup
\centering
![](imgs/patxipistolas.png){width="6.651cm" height="4.096cm" }\
\endgroup

Antiguamente la atracción se llamaba *Billy el Niño* pero hace algunos
años un joven, de puntería extraordinaria e incomparable rapidez hizo
los cincos aciertos en un tiempo record, 0.69 segundos. Desde ese día se
decidió cambiar el nombre de la atracción por el del joven pistolero.

¿Podrán los concursante más osados y rápidos cambiar el nombre de la
atracción como pasó hace años?

\newpage

## Entrada

El ejercicio comienza con un numero **n** de casos.
Cada caso contiene dos líneas. 
La primera con un número **p** que indica el número de participantes (entre 1 y 30). 
La segunda aparecen los **tiempos** de disparo de cada uno de los participantes en segundos. 

## Salida

El programa mostrará cuál es el mejor tiempo de los participantes (el menor).

## Entrada de ejemplo

>3\
>4\
>1.87 2.00 2.44 1.75\
>6\
>2.41 2.09 3.15 2.45 6.66 2.16\
>1\
>7.32\

## Salida de ejemplo

>1.75\
>2.09\
>7.32\

\newpage
# El Oasis de *Sirdeg*

Una de las atracciones más divertidas de nuestra feria es
el Oasis de *Sirdeg* y su famosa carrera de camellos. Esta
atracción consiste en averiguar cuál es el camello ganador de la
carrera.

\begingroup
\centering
![](imgs/camellos.png){width="6.481cm" height="3.63cm" style="float: center;margin-right: 7px;margin-top: 7px;"}\
\endgroup

Cada participante tiene **20 tiradas** para que su camello sea el
primero que alcance el otro extremo del oasis. Para hacerlo los
participantes tienen una pelota que deben ir introduciendo en unos
agujeros lanzándola desde lejos.

\begingroup
\centering ![](imgs/camellosagujeros.jpg){width="3.066cm" height="2.351cm"}\
\endgroup


Cada uno de los agujeros proporciona una serie de puntos :

-   Los agujeros de la franja verde son 1 punto.
-   Los agujeros de la franja azul son 2 puntos.
-   Los agujeros de la franja roja son 3 puntos.
-   Hay un agujero amarillo que no da puntos. (puntos 0)

\newpage

## Entrada

La primera línea nos dirá cuántos camellos **c** compiten en la carrera. A continuación aparecerán c líneas que representan las 20 tiradas de cada participante. 

## Salida

El programa mostrará el **número del camello** que gana la carrera.
*En el caso de que dos camellos o más lleguen en la misma tirada ganará el que tiene el número más bajo ya que se supone que participan en la carrera en ese orden.*
*Suponemos que **siempre** hay un camello que gana la carrera, o sea, que llega a los 20 puntos.*

## Entrada de ejemplo

>7\
>1 2 1 0 3 3 3 1 0 0 2 1 2 3 2 0 1 3 3 2\
>2 2 1 3 2 2 2 2 3 0 0 0 0 2 2 1 1 0 0 1\
>0 1 1 2 2 1 0 2 1 1 1 0 0 0 0 1 0 0 1 2\
>3 3 0 1 2 2 2 1 3 3 3 3 1 0 0 2 1 2 0 1\
>1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\
>2 2 2 2 1 1 1 0 3 1 0 0 1 1 2 3 2 1 3 3\
>1 2 1 0 3 3 3 1 0 0 2 1 2 3 2 0 1 3 3 2\

## Salida de ejemplo

>Camello 4\
