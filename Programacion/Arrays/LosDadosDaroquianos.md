---
title: Los dados Daroquianos
subtitle: Programación - IES La Sénia
author: Patxi Sanchis Luis
lang: es-ES
keywords: [PAF,IES,LaSenia]
titlepage: False
page-background: "../../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../../rsrc/backgrounds/background-title-senia.pdf"
---

# Los dados Daroquianos

Los *Daroquianos* son un pueblo nómada que se desplaza por todos los rincones del planeta llevando consigo, fundamentalmente, tres cosas: el trabajo duro, la alegría de vivir y los **dados Daroquianos**. Los dados Daroquianos aparentan ser dados normales de seis caras pero tienen la particularidad de que en sus caras sólo hay los números 1, 2 o 3. (tres caras con el 1, dos caras con el 2 y una cara con el 3).  

![Daroquianos](imgs/dadosDaroquianos.png){style="float: right;margin-right: 7px;margin-top: 7px;"} 

Estos dados son así debido a que cuando los Daroquianos tiraban los dados normales **nunca** sacaban más de un tres; por eso decidieron hacer dados que asumieran que nunca sacarían más de un tres, y así todos contentos.

\newpage
## Entrada 

Nuestra entrada empezará con un número que indica los casos de prueba. Para cada caso de prueba habrán dos líneas. 
Cada línea representan cinco tiradas de dados: la primera representan cinco tiradas con un dado **Daroquiano** y 
la segunda cinco tiradas con un dado normal.

## Salida

Mostrará como salida un marcador, que compara los resultados, de la forma **D-N**, donde **D** representa las veces que 
gana el dado **Daroquiano** y la **N** las veces que gana el dado normal. Los empates no se cuentan.

## Ejemplos

### Entrada de ejemplo

>3\
>1 1 2 1 3\
>4 2 6 5 2\
>3 3 2 3 1\
>3 2 1 2 1\
>1 1 1 2 1\
>1 4 2 2 6


### Salida de ejemplo

>1-4\
>3-0\
>0-3
