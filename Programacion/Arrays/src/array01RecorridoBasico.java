import java.util.*;

public class array01RecorridoBasico{
	
	public static void main (String args[]){
		
		// En este programa vamos a ver lo basico de
		// el recorrido en los vectores.
		
		// Definicion de un vector o array.
		int [] vectorEnteros = new int[10];
		
		// vamos a ver el contenido del vector
		// nada mas definirlo.
		
		System.out.println(" * Contenido del vector nada mas crearlo");
		System.out.print("[ ");
		for (int i=0;i<10;i++){
			// usaremos i para acceder a los diferentes
			// elementos del vector
			System.out.print(vectorEnteros[i]+","); 
		}	
		System.out.println("]");
		
		// Como se puede ver, si ejecutais el programa,
		// el Vector se inicializa a 0s.
		
		// Vamos ahora a establecer valores
		// propongo los numeros del 0 al 9
		
		vectorEnteros = new int[] {0,1,2,3,4,5,6,7,8,9};
		
		// esto parece un poco raro y lo es ^_^.
		
		System.out.println(" * Contenido del vector una vez relleno");
		System.out.print("[ ");
		for (int i=0;i<10;i++){
			// usaremos i para acceder a los diferentes
			// elementos del vector
			System.out.print(vectorEnteros[i]+","); 
		}	
		System.out.println("]");
		
		// Ahora vamos hacer que el el usuario rellene el array 
		// usando Scanner de manera interactiva.
		
		// Rellenar por parte del alumnado
		
		
		
		
		
		
		
		
		
		
		// Aqui se vuelve a mostrar el contenido
		System.out.println(" * Contenido del vector una vez relleno por el alumnado");
		System.out.print("[ ");
		for (int i=0;i<10;i++){
			// usaremos i para acceder a los diferentes
			// elementos del vector
			System.out.print(vectorEnteros[i]+","); 
		}	
		System.out.println("]");
		
	}
}

