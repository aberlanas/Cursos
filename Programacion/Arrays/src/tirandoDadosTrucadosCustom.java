import java.util.*;
import java.math.*;

public class tirandoDadosTrucadosCustom{
	
	public static void main (String args[]){
		
		// Creamos un Scanner para introducir el numero
		// de dados por entrada estandar 
		Scanner sc = new Scanner(System.in);
		
		// Obtenemos el numero de dados que deseamos tirar
		System.out.print(" * Indica el numero de dados que deseas lanzar -> ");
		int numDados = sc.nextInt();
		
		// Ahora vamos a ampliar un poco el ejercicio anterior
		// en vez de considerar 5 y 6 como exitos, vamos 
		// a preguntarle al usuario que indique el numero 
		// que ya nos va a valer como exito.
		// si nos dice el 3 por ejemplo
		// deberemos mostrar todos los numeros mayores e iguales
		// a 3 con un asterisco.
		
		System.out.print(" * Indica el numero a partir del cual debe ser considerado un exito ");
		int exito = sc.nextInt();
		
		
		// Comprueba que el exito no es mas grande que 6
		// en caso de que lo sea, pidele al usuario que 
		// sea como mucho 6 
		// mientras no sea correcto
		// que se lo siga pidiendo.
		
		int resultado = 0;
		
		// Ahora realizamos las tiradas
		for (int i = 0; i< numDados; i++){
			System.out.print(" Tiro un dado de 6 Caras : ");
			
			// Esto lo veremos el lunes en clase
			// pero basicamente consiste en obtener un 
			// numero entero aleatorio entre el 1 y 6.
			// para ello se utiliza el metodo Math.random() 
			// que si lo bicheais (curioseais),
			// vereis que devuelve un numero decimal
			// aleatorio entre el 0.0 y 1.0 (sin llegar nunca al 1.0).
			
			
			resultado = (int)Math.floor(Math.random() * 6) + 1;
			
			// Mostramos el resultado
			System.out.print(resultado);
			
			// Y si el resultado es mayor o igual que el exito, debemos mostrar un asterisco
			// a continuacion del resultado.
			// en caso contrario vamos con la siguiente tirada
			if (resultado <=4) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

