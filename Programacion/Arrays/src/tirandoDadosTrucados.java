import java.util.*;
import java.math.*;

public class tirandoDadosTrucados{
	
	public static void main (String args[]){
		
		// Creamos un Scanner para introducir el numero
		// de dados por entrada estandar 
		Scanner sc = new Scanner(System.in);
		
		// Obtenemos el numero de dados que deseamos tirar
		System.out.print(" * Indica el numero de dados que deseas lanzar -> ");
		int numDados = sc.nextInt();
		
		int resultado = 0;
		
		// Ahora realizamos las tiradas
		for (int i = 0; i< numDados; i++){
			System.out.print(" Tiro un dado de 6 Caras : ");
			
			// Esto lo veremos el lunes en clase
			// pero basicamente consiste en obtener un 
			// numero entero aleatorio entre el 1 y 6.
			// para ello se utiliza el metodo Math.random() 
			// que si lo bicheais (curioseais),
			// vereis que devuelve un numero decimal
			// aleatorio entre el 0.0 y 1.0 (sin llegar nunca al 1.0).
			
			
			resultado = (int)Math.floor(Math.random() * 6) + 1;
			
			// Mostramos el resultado
			System.out.print(resultado);
			
			// Y si el resultado es 5 o 6, debemos mostrar un asterisco
			// a continuacion del resultado.
			// en caso contrario vamos con la siguiente tirada
			if (resultado <=4) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

