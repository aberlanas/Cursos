import java.util.*;
import java.math.*;

public class array02RecorridoBasicoRandom{
	
	public static void main (String args[]){
		
		// En este programa vamos a ver lo basico de
		// el recorrido en los vectores.
		
		// Definicion de un vector o array.
		int [] vectorEnteros = new int[20];
		
		// vamos a ver el contenido del vector
		// nada mas definirlo.
		
		// vamos a mejorar el recorrido de los vectores
		// en vez de poner el numero de items del vector
		// vamos a usar una propiedad que tienen los vectores
		// que se puede consultar que es 
		
		// length
		
		// se accede mediante la notacion
		System.out.println(" * Contenido del vector nada mas crearlo");
		System.out.print("[ ");
		for (int i=0;i<vectorEnteros.length;i++){
			// usaremos i para acceder a los diferentes
			// elementos del vector
			System.out.print(vectorEnteros[i]+","); 
		}	
		System.out.println("]");
		
		// vamos a usar ahora lo aprendido con Random
		// para llenar el vector con los numeros del 1 al 1517
		for (int i=0;i<vectorEnteros.length;i++){
			vectorEnteros[i]=(int)Math.floor(Math.random() * 1517) + 1;
		}
		
		
		System.out.println(" * Contenido del vector una vez relleno");
		System.out.print("[ ");
		for (int i=0;i<vectorEnteros.length;i++){
			// usaremos i para acceder a los diferentes
			// elementos del vector
			System.out.print(vectorEnteros[i]+","); 
		}	
		System.out.println("]");
		
		// Tarea para el alumnado
		// Obten el numero mas alto que se ha insertado en el vector.
		// tened en cuenta que debereis recorrer el vector pasando
		// por todos los elementos.
		
		
		// Tarea para el alumnado 
		// Obten el numero mas bajo que se ha insertado en el vector
		
		
		// Muestra ambos resultados (el maximo y el minimo).
		// maximo = valor mas alto
		// minimo = valor mas bajo.

		
		
	}
}
