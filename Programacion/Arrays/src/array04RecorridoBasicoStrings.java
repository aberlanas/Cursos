import java.util.*;
import java.math.*;

public class array04RecorridoBasicoStrings{
	
	public static void main (String args[]){
		
		String frase = "el veloz murcielago hindu comia feliz cardillo y kiwi";

	
		// Realmente los Strings se pueden tratar como arrays.
		// por que son un tipo de dato que se comportan de una 
		// manera muy similar.
		
		// ademas Java nos permite acceder desde la clase String 
		// a los diferente elementos de un String indicando su posicion dentro 
		// de la cadena.
		// Esto mostrara la primera letra de la frase.
		System.out.println(frase.charAt(0));
		
		// Esto nos mostrara la 'z'
		System.out.println(frase.charAt(7));
		
		// Esto la ultima letra
		System.out.println(frase.charAt(frase.length()-1));
		
		// Y esto lo podemos usar en un for como 
		// motor de recorrido sin problema
		
		for (int ci=0;ci<frase.length();ci++){
			System.out.println(frase.charAt(ci));
		}
		
		// Tareas para el alumnado
		
		// 1.Cuenta el numero de vocales que aparecen en la frase.
		
		// 2.Muestra en una unica linea la frase leida de derecha a izquierda (al reves)
		
		// 3. Cuenta el numero de espacios que aparecen en la frase
		
		// 4. Crea un array de Caracteres de longitud frase.length()-espacios y 
		// copia a ese array la frase sin espacios.
		
		// 5. Imprime por pantalla el array de characteres resultante.
	}
}