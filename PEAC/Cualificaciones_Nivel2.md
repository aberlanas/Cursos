---
title: PEAC - IFC Nivel 3
subtitle: "Grados Superiores"
author: Consejo PEAC IES La Senia
header-includes: |
lang: es-ES
keywords: [PEAC,IFC,Nivel3]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia-peac.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-peac.pdf"
---



# Introducción

A continuación se muestran una serie de Cualifaciones y su Competencia General.

Este documento pretende ser un apoyo a la hora de tomar decisiones en cuanto a 
la solicitud de la adquisicón de Competencias por parte de los/las candidat@s.

# IFC079\_3 - Administración de Bases de datos

Administrar un sistema de bases de datos en sistemas informáticos, interpretando su diseño y estructura, adaptando el modelo a los requerimientos del sistema gestor de bases de datos, y configurándolo a fin de asegurar la integridad, disponibilidad y confidencialidad de la información almacenada, cumpliendo la normativa específica de protección de datos, la planificación de la actividad preventiva y los estándares de calidad.

## Unidades de Competencia

 * UC0223_3 - Configurar y explotar sistemas informáticos
 * UC0224_3 - Instalar y administrar sistemas gestores de bases de datos
 * UC0225_3 - Configurar y gestionar bases de datos relacionales
 * UC2318_3 - Instalar, configurar y administrar la capa de virtualización de los sistemas y redes sobre los que se ofrece el servicio

---

# IFC080\_3 - Programación con lenguajes orientados a objetos y bases de datos relacionales

Desarrollar aplicaciones informáticas integrándolas en diferentes sistemas, plataformas y tecnologías acorde al diseño especificado para el tratamiento de la información utilizando lenguajes orientados a objetos y bases de datos relacionales manteniendo su integridad y consistencia, optimizando los recursos materiales y humanos de que se dispone, tanto propios como externos, cumpliendo la normativa relativa a planificación de la actividad preventiva y de aplicación en el sector de desarrollo de software y estándares de calidad.

## Unidades de Competencia

 * UC0223_3 - Configurar y explotar sistemas informáticos
 * UC0226_3 - Programar bases de datos relacionales
 * UC0227_3 - Desarrollar componentes 'software' en lenguajes de programación orientados a objetos

---

# IFC081\_3 - Administración y Diseño de Redes Departamentales

Diseñar la arquitectura de comunicaciones de un entorno de complejidad media o baja, supervisar su implantación siguiendo el proyecto y administrar el sistema resultante, proporcionando la asistencia técnica necesaria.

## Unidades de Competencia

 * UC0228_3 - Diseñar la infraestructura de red telemática
 * UC0229_3 - Coordinar la implantación de la infraestructura de red telemática
 * UC0230_3 - Administrar la infraestructura de red telemática

---

# IFC152\_3 - Gestión de sistemas informáticos

Configurar, administrar y mantener un sistema informático a nivel de hardware y software, garantizando la disponibilidad, óptimo rendimiento, funcionalidad e integridad de los servicios y recursos del sistema.

## Unidades de Competencia

 * UC0484_3 - Administrar los dispositivos hardware del sistema
 * UC0485_3 - Instalar, configurar y administrar el software de base y de aplicación del sistema
 * UC0486_3 - Asegurar equipos informáticos
 
---

# IFC153\_3 - Seguridad informática

Garantizar la seguridad de los accesos y usos de la información registrada en equipos informáticos, así como del propio sistema, protegiéndolos de los posibles ataques, identificando vulnerabilidades y aplicando sistemas de cifrado a las comunicaciones que se realicen hacia el exterior y en el interior de la organización.

## Unidades de Competencia

 * UC0486_3 - Asegurar equipos informáticos
 * UC0487_3 - Auditar redes de comunicación y sistemas informáticos
 * UC0488_3 - Detectar y responder ante incidentes de seguridad
 * UC0489_3 - Diseñar e implementar sistemas seguros de acceso y transmisión de datos
 * UC0490_3 - Gestionar servicios en el sistema informático

---

# IFC154\_3 - Desarrollo de aplicaciones con tecnologías web

Desarrollar documentos y componentes software que constituyan aplicaciones informáticas en entornos distribuidos utilizando tecnologías Web, partiendo de una especificación técnica ya elaborada, realizando, además, la verificación, documentación e implantación de los mismos.

## Unidades de Competencia

 * UC0491_3 - Desarrollar elementos software en el entorno cliente
 * UC0492_3 - Desarrollar elementos software en el entorno servidor
 * UC0493_3 - Implementar, verificar y documentar aplicaciones web en entornos internet, intranet y extranet

---

# IFC155_3 - Programación en lenguajes estructurados de aplicaciones de gestión

Desarrollar aplicaciones de gestión a partir de un diseño especificado, mediante técnicas de programación estructurada, utilizando equipos y herramientas informáticas, accediendo y manipulando la información ubicada en sistemas gestores de bases de datos.

## Unidades de Competencia

 * UC0223_3 - Configurar y explotar sistemas informáticos 
 * UC0226_3 - Programar bases de datos relacionales
 * UC0494_3 - Desarrollar componentes software en lenguajes de programación estructurada

---

# IFC156_3 - Administración de servicios de Internet

Instalar, configurar, administrar y mantener servicios de provisión e intercambio de información utilizando los recursos de comunicaciones que ofrecen las tecnologías de Internet, cumpliendo la normativa relativa a protección medioambiental y la planificación de la actividad preventiva, asegurando la disponibilidad, integridad y confidencialidad de la información publicada, cumpliendo la normativa específica de publicación de la información y de protección de datos y los estándares de calidad.

## Unidades de Competencia

 * UC0495_3 - Instalar, configurar y administrar el software para gestionar un entorno web
 * UC0496_3 - Instalar, configurar y administrar servicios de mensajería electrónica
 * UC0497_3 - Instalar, configurar y administrar servicios de transferencia de archivos y multimedia
 * UC0490_3 - Gestionar servicios en el sistema informático
 * UC2318_3 - Instalar, configurar y administrar la capa de virtualización de los sistemas y redes sobre los que se ofrece el servicio

---

# IFC302_3 - Gestión de redes de voz y datos

Definir y supervisar los procedimientos de instalación, configuración y mantenimiento de los recursos de una red de comunicaciones para proveer servicios de voz, datos y multimedia a los usuarios y realizar la integración de los recursos ofrecidos por los sistemas de transmisión y conmutación


## Unidades de Competencia

 * UC0228_3 - Diseñar la infraestructura de red telemática
 * UC0962_3 - Integrar servicios de voz, datos y multimedia
 * UC0963_3 - Administrar y proveer servicios de comunicaciones a usuarios

---


# IFC303_3 - Programación de sistemas informáticos

Desarrollar componentes software a partir de unas especificaciones concretas, proporcionando funciones de administración y supervisión del sistema operativo, para la gestión de los recursos de un sistema informático y la interacción con otros sistemas utilizando tecnologías de desarrollo orientadas a objetos y a componentes.

## Unidades de Competencia

 * UC0964_3 - Crear elementos software para la gestión del sistema y sus recursos
 * UC0965_3 - Desarrollar elementos software con tecnologías de programación basada en componentes

---

# IFC304_3 - Sistemas de gestión de información

Implementar y administrar sistemas de gestión de información en una organización, según un diseño especificado, ubicados en plataformas y soportes informáticos heterogéneos que garanticen su registro, seguridad, clasificación, distribución y trazabilidad.

## Unidades de Competencia 


 * UC0966_3 - Consultar y extraer información de distintas plataformas de almacenamiento de datos
 * UC0967_3 - Crear y gestionar repositorios de contenidos
 * UC0968_3 - Administrar el sistema de gestión de información

---

# IFC363_3 - Administración y programación en sistemas de planificación de recursos empresariales y de gestión de relaciones con clientes

Instalar, configurar y administrar sistemas de planificación de recursos empresariales y de gestión de relaciones con los clientes (sistemas ERP-CRM: Enterprise Resource Planning - Customer Relationship Management), adecuándolos mediante la programación de componentes software, a partir de unas especificaciones de diseño, con el fin de soportar las reglas de negocio de la organización, y asegurando su funcionamiento dentro de los parámetros organizativos de la empresa.

## Unidades de Competencia


 * UC1213_3 - Instalar y configurar sistemas de planificación de recursos empresariales y de gestión de relaciones con clientes
 * UC1214_3 - Administrar sistemas de planificación de recursos empresariales y de gestión de relaciones con clientes
 * UC1215_3 - Realizar y mantener componentes software en un sistema de planificación de recursos empresariales y de gestión de relaciones con clientes

---

# IFC364_3 - Gestión y supervisión de alarmas en redes de comunicaciones

Supervisar y gestionar la red de comunicaciones, resolviendo incidencias en los sistemas de comunicaciones, reprogramando el encaminamiento de tráfico y manteniendo la calidad en los servicios, siguiendo las especificaciones establecidas por la organización.

## Unidades de Competencia


 * UC1216_3 - Monitorizar el estado y la disponibilidad de la red de comunicaciones y de los servicios implementados
 * UC1217_3 - Realizar operaciones de configuración y de control de la red de comunicaciones
 * UC1218_3 - Gestionar la calidad de los servicios soportados sobre la red de comunicaciones

----

# IFC365_3 - Implantación y gestión de elementos informáticos en sistemas domóticos-inmóticos, de control de accesos y presencia, y de videovigilancia

Integrar y mantener elementos informáticos y de comunicaciones en sistemas de automatización de edificios domóticos e inmóticos, de control de accesos y presencia y de videovigilancia a nivel de hardware y software, asegurando el funcionamiento de los distintos módulos que los componen, en condiciones de calidad y seguridad, cumpliendo la normativa y reglamentación aplicables.

## Unidades de Competencia 

 * UC0490_3 - Gestionar servicios en el sistema informático
 * UC1219_3 - Implantar y mantener sistemas domóticos/inmóticos
 * UC1220_3 - Implantar y mantener sistemas de control de accesos y presencia, y de videovigilancia

# IFC366_3 - Mantenimiento de segundo nivel en sistemas de radiocomunicaciones

Organizar y coordinar los procesos de implementación y de mantenimiento preventivo, así como resolver las incidencias y reclamaciones recibidas directamente o escaladas por el nivel inferior, en redes inalámbricas de área local y metropolitana, y en sistemas de radiocomunicaciones fijas y móviles, asegurando su disponibilidad, seguridad, óptimo rendimiento y funcionalidad de los servicios.

## Unidades de Competencia


 * UC1221_3 - Organizar y gestionar la puesta en servicio y el mantenimiento de redes inalámbricas de área local y metropolitanas
 * UC1222_3 - Coordinar la puesta en servicio de sistemas de radiocomunicaciones de redes fijas y móviles
 * UC1223_3 - Gestionar el mantenimiento de sistemas de radiocomunicaciones de redes fijas y móviles

----


# IFC749_3 - Gestión de datos y entrenamiento en sistemas de Inteligencia Artificial basados en aprendizaje automático.

Extraer, procesar y aplicar analíticas de datos para el entrenamiento en sistemas de Inteligencia Artificial, basándose en técnicas de aprendizaje automático para la predicción, clasificación o cualquier otro tratamiento inteligente de datos, imágenes, vídeos o lenguaje natural, preprocesando, depurando y particionando los datos en subconjuntos, identificando las variables significativas, verificando la ausencia de sesgos y cumpliendo la normativa aplicable en materia de protección de datos y propiedad intelectual e industrial.

## Unidades de Competencia


 * UC0966_3 - Consultar y extraer información de distintas plataformas de almacenamiento de datos
 * UC2492_3 - Procesar los datos para su uso en sistemas de inteligencia artificial basados en aprendizaje automático
 * UC2493_3 - Entrenar modelos en sistemas de inteligencia artificial basados en aprendizaje automático

----


# IFC750_3 - Gestión de la instalación, despliegue y explotación de sistemas de Inteligencia Artificial basados en aprendizaje automático.

Instalar, configurar, desplegar y mantener herramientas y software en sistemas informáticos de Inteligencia Artificial, basándose en técnicas de aprendizaje automático para la predicción, clasificación o cualquier otro tratamiento inteligente de datos, imágenes, vídeos o lenguaje natural, preprocesando, depurando y particionando los datos en subconjuntos, identificando las variables significativas, verificando la ausencia de sesgos y cumpliendo la normativa aplicable en materia de protección de datos y propiedad intelectual e industrial.


## Unidades de Competencia 


 * UC2494_3 - Instalar y mantener sistemas de inteligencia artificial basados en aprendizaje automático
 * UC2495_3 - Desplegar sistemas de inteligencia artificial basados en aprendizaje automático
 * UC2496_3 - Explotar servicios de procesamiento y analítica de datos en plataformas disponibles en línea
 * UC2497_3 - Desarrollar componentes software específicos para sistemas de inteligencia artificial basados en aprendizaje automático


---

# IFC789_3 - Desarrollo de productos basados en cadenas de bloques Blockchain

Desarrollar y desplegar productos basados en tecnologías de cadenas de bloques, Blockchain para el registro fehaciente de transacciones descritas mediante Contratos Inteligentes ('Smart Contracts') y para su gestión con interfaces de usuario ('frontend'), cumpliendo la normativa aplicable de protección de datos, seguridad, propiedad intelectual e industrial y la planificación de la actividad preventiva, así como los estándares de calidad.

## Unidades de Competencia


 * UC2630_3 - Preparar herramientas de desarrollo de productos que utilicen tecnologías descentralizadas blockchain
 * UC2631_3 - Programar contratos inteligentes ('smart contracts')
 * UC2632_3 - Desarrollar interfaces de usuario 'frontend' para interacción con redes descentralizadas basadas en blockchain
 * UC2633_3 - Desplegar contratos inteligentes ('smart contracts') sobre nodos de red dlt, blockchain

\newpage

# Nivel Grado Medio (Nivel 2)

IFC078_2 - Sistemas microinformáticos

Técnico en Sistemas Microinformáticos y Redes

IFC297_2 - Confección y publicación de páginas web

No existe ningún Título que abarque la Cualificación completa

IFC298_2 - Montaje y reparación de sistemas microinformáticos

Técnico en Sistemas Microinformáticos y Redes

IFC299_2 - Operación de redes departamentales

Técnico en Sistemas Microinformáticos y Redes

IFC300_2 - Operación de sistemas informáticos

Técnico en Sistemas Microinformáticos y Redes

IFC301_2 - Operación en sistemas de comunicaciones de voz y datos

No existe ningún Título que abarque la Cualificación completa

IFC362_2 - Operaciones de mantenimiento de sistemas de radiocomunicaciones

No existe ningún Título que abarque la Cualificación completa

IFC748_2 - Digitalización aplicada al entorno profesional 

No existe ningún Título que abarque la Cualificación completa

\newpage
# Grados Superiores (Nivel 3)


IFC079_3 - Administración de bases de datos

Técnico Superior en Administración de Sistemas Informáticos en Red

IFC080_3 - Programación con lenguajes orientados a objetos y bases de datos relacionales

Técnico Superior en Desarrollo de Aplicaciones Web
Técnico Superior en Desarrollo de Aplicaciones Multiplataforma

IFC081_3 - Administración y diseño de redes departamentales

No existe ningún Título que abarque la Cualificación completa

IFC152_3 - Gestión de sistemas informáticos

Técnico Superior en Administración de Sistemas Informáticos en Red

IFC153_3 - Seguridad informática

No existe ningún Título que abarque la Cualificación completa

IFC154_3 - Desarrollo de aplicaciones con tecnologías web

Técnico Superior en Desarrollo de Aplicaciones Web

Técnico Superior en Administración de Sistemas Informáticos en Red

IFC155_3 - Programación en lenguajes estructurados de aplicaciones de gestión

Técnico Superior en Desarrollo de Aplicaciones Web

Técnico Superior en Desarrollo de Aplicaciones Multiplataforma

IFC156_3 - Administración de servicios de Internet

Técnico Superior en Administración de Sistemas Informáticos en Red

IFC302_3 - Gestión de redes de voz y datos

No existe ningún Título que abarque la Cualificación completa

IFC303_3 - Programación de sistemas informáticos

Técnico Superior en Desarrollo de Aplicaciones Multiplataforma

IFC304_3 - Sistemas de gestión de información

No existe ningún Título que abarque la Cualificación completa

IFC363_3 - Administración y programación en sistemas de planificación de recursos empresariales y de gestión de relaciones con clientes

Técnico Superior en Desarrollo de Aplicaciones Multiplataforma

IFC364_3 - Gestión y supervisión de alarmas en redes de comunicaciones

No existe ningún Título que abarque la Cualificación completa

IFC365_3 - Implantación y gestión de elementos informáticos en sistemas domóticos-inmóticos, de control de accesos y presencia, y de videovigilancia

No existe ningún Título que abarque la Cualificación completa

IFC366_3 - Mantenimiento de segundo nivel en sistemas de radiocomunicaciones

No existe ningún Título que abarque la Cualificación completa

IFC749_3 - Gestión de datos y entrenamiento en sistemas de Inteligencia Artificial basados en aprendizaje automático.

No existe ningún Título que abarque la Cualificación completa

IFC750_3 - Gestión de la instalación, despliegue y explotación de sistemas de Inteligencia Artificial basados en aprendizaje automático.ç

No existe ningún Título que abarque la Cualificación completa

IFC789_3 - Desarrollo de productos basados en cadenas de bloques Blockchain 

No existe ningún Título que abarque la Cualificación completa


