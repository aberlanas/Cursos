---
title: "UD07 Kanban y Organización en el trabajo - Tarea Opcional"
subtitle: "Herramientas de Creación de Vídeotutoriales para Aprendizaje Basado en Servicios"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ABS, Didáctica]
titlepage: true,
page-background: "../rsrc/backgrounds/background-cefire.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title.pdf"
---

# Tarea 01

Esta tarea es **opcional**, aunque animo a tod@s a realizarla.

Ved el vídeo que está colgado en la plataforma y cread un Tablero Trello donde se muestre una posible subdivisión en tareas del proyecto que presentasteis en la Unidad 01 (u otro que os pueda ser útil).

Algunas propuestas que podéis incluir:

- Diferentes usuarios.
- Etiquetas personalizadas.
- Columnas diferentes.

Realizad una captura de pantalla y subirla a la plataforma.
