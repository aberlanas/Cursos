import java.util.*;

public class p355{
	
	public static void main (String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int anyo = 0;
		
		while (casos > 0){
			anyo = sc.nextInt();
			//System.out.println(anyo);
			if (anyo % 4 == 0){
				if (anyo % 100 == 0 && anyo % 400 !=0){
					System.out.println("28");
				}else{
					System.out.println("29");
				}
			}else{
				System.out.println("28");
			}
			
			casos--;
		}
	}
}