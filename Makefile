#!/usr/bin/make -f

#TEMPLATE_TEX_PD="rsrc/templates/pd-nologo-tpl.latex"
# Colors
BLUE= \e[1;34m
LIGHTBLUE= \e[94m
LIGHTGREEN= \e[92m
LIGHTYELLOW= \e[93m

RESET= \e[0m

# Templates 
TEMPLATE_TEX_PD="../rsrc/templates/eisvogel.tex"
PANDOC_OPTIONS="-V fontsize=10pt -V mainfont="Ubuntu" --pdf-engine=xelatex "
TEMPLATE_TEX_TASK="../rsrc/templates/eisvogel.tex"

# PDFS
PDF_PATH:=$(shell readlink -f PDFS)

# Dirs 
PAF2022_DIR:=$(shell readlink -f PAF-CompetenciaDigital-LaSenia-2022/)
PAF2022_FILES := $(wildcard $(PAF2022_DIR)/*.md)

SEC2022_DIR=$(shell readlink -f 2022-SeguridadInformatica/)
SEC2022_FILES := $(wildcard $(SEC2022_DIR)/*.md)

PROGARRAYS_DIR=$(shell readlink -f Programacion/Arrays/)
PROGARRAYS_FILES:=$(wildcard $(PROGARRAYS_DIR)/*.md)

PROGPOO_DIR=$(shell readlink -f Programacion/POO/)
PROGPOO_FILES:=$(wildcard $(PROGPOO_DIR)/*.md)

PEAC_DIR:=$(shell readlink -f PEAC)
PEAC_FILES:=$(wildcard $(PEAC_DIR)/*.md)

MEMORIAS_DIR:=$(shell readlink -f Memoria-CoordTIC)
MEMORIAS_FILES:=$(wildcard $(MEMORIAS_DIR)/*.md)


# RULES

clean:
	@echo " [${BLUE} * Step : Clean ${RESET}] "
	@echo "${LIGHTBLUE} -- PDFS ${RESET}"
	rm -f PDFS/*.pdf
	rm -f PDFS/*.odt


files:
	@echo " [${BLUE} * Step : Files ${RESET}] "
	@echo "${LIGHTBLUE} * Creating folder [ PDFS ]${RESET}"
	mkdir -p PDFS

paf-2022: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} PAF - 2022 ${RESET}] "

	@for f in $(PAF2022_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(PAF2022_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done


programacion-arrays: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Programacion - Arrays ${RESET}] "

	@for f in $(PROGARRAYS_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(PROGARRAYS_DIR) && pandoc $$f --template ../$(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done

programacion-poo: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Programacion - POO ${RESET}] "

	@for f in $(PROGPOO_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(PROGPOO_DIR) && pandoc $$f --template ../$(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done


peac: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} PEAC ${RESET}] "

	@for f in $(PEAC_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(PEAC_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done



memorias: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} MEMORIAS ${RESET}] "

	@for f in $(MEMORIAS_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(MEMORIAS_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done







seguridad-2022-1: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Seguridad Informatica ${RESET}] "

	@for f in $(SEC2022_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(SEC2022_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done
